
Описание проекта Stogramm
--------------------------------------------

Андроид приложение - просмотр коротких видео (аналог ТикТок, вайн, ютуб шортс). Есть возможность создать профиль и загружать свои видео. Также, возможность поставить лайк на видео, подписаться на автора и просматривать видео подписок.
Проект выполнялся в рамках практики в Tinkoff.

--------------------------------------------
Особенности реализации
--------------------------------------------
Архитектура приложения: MVVM

Навигация: Jetpack Navigation

DI: dagger-hilt

Библиотеки:
- *Glide* - загрузка изображений;
- *ExoPlayer* - загрузка и отображение видео;
- *Retrofit2 + OkHttp* - работа с сетью;
- *Auth0-jwtdecode* - работа с JWT токенами;

--------------------------------------------
Демонстрация работы
--------------------------------------------

### Регистрация

<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="https://i.imgur.com/Zbannep.mp4" type="video/mp4">
  </video>
</figure>

Ссылка если не работает видео: https://imgur.com/a/3Ktals0

### Авторизация

<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="https://i.imgur.com/bNKAVWJ.mp4" type="video/mp4">
  </video>
</figure>

Ссылка если не работает видео: https://imgur.com/a/56CRGMJ

### Лайк+подписка

<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="https://i.imgur.com/64S1seC.mp4" type="video/mp4">
  </video>
</figure>

Ссылка если не работает видео: https://imgur.com/a/wu4aHzi

### Просмотр видео автора + отписка

<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="https://i.imgur.com/9GP96hy.mp4" type="video/mp4">
  </video>
</figure>

Ссылка если не работает видео: https://imgur.com/a/WC0CaCo

### Загрузка видео

<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="https://i.imgur.com/BGJ7jCi.mp4" type="video/mp4">
  </video>
</figure>

Ссылка если не работает видео: https://imgur.com/a/WBZOLct

