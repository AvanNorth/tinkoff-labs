package com.pskda.tinkofflabs.di

import com.pskda.tinkofflabs.data.AuthRepositoryImpl
import com.pskda.tinkofflabs.data.ProfileRepositoryImpl
import com.pskda.tinkofflabs.data.VideoRepositoryImpl
import com.pskda.tinkofflabs.data.api.AuthApi
import com.pskda.tinkofflabs.data.api.UserApi
import com.pskda.tinkofflabs.data.api.mapper.ContentInfoMapper
import com.pskda.tinkofflabs.data.api.mapper.VideoMapper
import com.pskda.tinkofflabs.data.session.UserPreferences
import com.pskda.tinkofflabs.domain.repository.AuthRepository
import com.pskda.tinkofflabs.domain.repository.ProfileRepository
import com.pskda.tinkofflabs.domain.repository.VideoRepository
import com.pskda.tinkofflabs.domain.usecase.AuthUseCase
import com.pskda.tinkofflabs.domain.usecase.VideoUseCase
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Provides
    @Singleton
    fun provideVideoMapper(): VideoMapper = VideoMapper()

    @Provides
    @Singleton
    fun provideInfoMapper(): ContentInfoMapper = ContentInfoMapper()
}