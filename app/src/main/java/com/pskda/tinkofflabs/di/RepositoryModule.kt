package com.pskda.tinkofflabs.di

import com.pskda.tinkofflabs.data.AuthRepositoryImpl
import com.pskda.tinkofflabs.data.ProfileRepositoryImpl
import com.pskda.tinkofflabs.data.UploadRepositoryImpl
import com.pskda.tinkofflabs.data.VideoRepositoryImpl
import com.pskda.tinkofflabs.domain.repository.AuthRepository
import com.pskda.tinkofflabs.domain.repository.ProfileRepository
import com.pskda.tinkofflabs.domain.repository.UploadRepository
import com.pskda.tinkofflabs.domain.repository.VideoRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    abstract fun provideVideoRepository(videoRepositoryImpl: VideoRepositoryImpl): VideoRepository

    @Binds
    abstract fun provideAuthRepository(authRepositoryImpl: AuthRepositoryImpl): AuthRepository

    @Binds
    abstract fun provideProfileRepository(profileRepositoryImpl: ProfileRepositoryImpl): ProfileRepository

    @Binds
    abstract fun provideUploadRepository(uploadRepositoryImpl: UploadRepositoryImpl): UploadRepository
}