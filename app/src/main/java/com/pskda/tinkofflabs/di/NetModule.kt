package com.pskda.tinkofflabs.di

import android.content.Context
import androidx.viewbinding.BuildConfig
import com.pskda.tinkofflabs.data.api.AuthApi
import com.pskda.tinkofflabs.data.api.UploadApi
import com.pskda.tinkofflabs.data.api.UserApi
import com.pskda.tinkofflabs.data.api.VideoApi
import com.pskda.tinkofflabs.data.interceptors.AuthInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

private const val BASE_URL = "https://stogramm.herokuapp.com/"

@Module
@InstallIn(SingletonComponent::class)
class NetModule {
    @Provides
    @LoggingInterceptor
    fun provideLoggingInterceptor(): Interceptor {
        return HttpLoggingInterceptor()
            .setLevel(
                HttpLoggingInterceptor.Level.HEADERS
            )
    }

    @Provides
    @Singleton
    fun provideTokenInterceptor(@ApplicationContext context: Context): AuthInterceptor =
        AuthInterceptor(context)

    @Provides
    fun provideGsonConverter(): GsonConverterFactory = GsonConverterFactory.create()

    @Provides
    @Singleton
    fun provideAuthApi(
        gsonConverter: GsonConverterFactory,
        @LoggingInterceptor loggingInterceptor: Interceptor
    ): AuthApi =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(OkHttpClient.Builder().addInterceptor(loggingInterceptor).build())
            .addConverterFactory(gsonConverter)
            .build()
            .create(AuthApi::class.java)

    @Provides
    @Singleton
    fun provideUserApi(
        gsonConverter: GsonConverterFactory,
        authInterceptor: AuthInterceptor,
        @LoggingInterceptor loggingInterceptor: Interceptor
    ): UserApi =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(OkHttpClient.Builder().addInterceptor(authInterceptor).addInterceptor(loggingInterceptor).build())
            .addConverterFactory(gsonConverter)
            .build()
            .create(UserApi::class.java)

    @Provides
    @Singleton
    fun provideUploadApi(
        gsonConverter: GsonConverterFactory,
        authInterceptor: AuthInterceptor,
        @LoggingInterceptor loggingInterceptor: Interceptor
    ): UploadApi =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(OkHttpClient.Builder().addInterceptor(authInterceptor).addInterceptor(loggingInterceptor).build())
            .addConverterFactory(gsonConverter)
            .build()
            .create(UploadApi::class.java)

    @Provides
    @Singleton
    fun provideVideoApi(
        gsonConverter: GsonConverterFactory,
        authInterceptor: AuthInterceptor,
        @LoggingInterceptor loggingInterceptor: Interceptor
    ): VideoApi =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(OkHttpClient.Builder().addInterceptor(authInterceptor).addInterceptor(loggingInterceptor).build())
            .addConverterFactory(gsonConverter)
            .build()
            .create(VideoApi::class.java)

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class LoggingInterceptor
}