package com.pskda.tinkofflabs.data.interceptors

import android.content.Context
import com.pskda.tinkofflabs.data.session.UserPreferences
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(context: Context) : Interceptor {
    var userPrefs: UserPreferences = UserPreferences(context)
    private var token: String? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()

        runBlocking {
            token = userPrefs.authToken.first()
        }
        if (token != null) {
            requestBuilder.addHeader("Authorization", "Bearer $token")
        }

        return chain.proceed(requestBuilder.build())
    }
}