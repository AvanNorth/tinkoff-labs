package com.pskda.tinkofflabs.data

import com.pskda.tinkofflabs.data.api.UserApi
import com.pskda.tinkofflabs.data.api.model.SubscribeResponse
import com.pskda.tinkofflabs.data.api.model.User
import com.pskda.tinkofflabs.domain.repository.ProfileRepository
import javax.inject.Inject

class ProfileRepositoryImpl @Inject constructor(
    private val api: UserApi,
) : ProfileRepository {
    override suspend fun getProfileById(id: String): User = api.getProfile(id)

    override suspend fun subscribeToUser(id: Long): User = api.subscribeById(id)

    override suspend fun unsubscribeFromUser(id: Long): User = api.unsubscribeById(id)
}