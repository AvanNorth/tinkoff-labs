package com.pskda.tinkofflabs.data.api.model

import com.google.gson.annotations.SerializedName

class LikeResponse(
    @SerializedName("likesCount")
    var likes: Long
)