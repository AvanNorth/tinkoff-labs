package com.pskda.tinkofflabs.data.api.model

import com.google.gson.annotations.SerializedName

data class PostListResponse(
    @SerializedName("posts")
    var videoList: MutableList<PostResponse>,
    @SerializedName("totalPages")
    var totalPages: Int,
    @SerializedName("currentPage")
    var currentPage: Int
)
