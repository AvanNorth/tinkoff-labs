package com.pskda.tinkofflabs.data.api.model

import com.google.gson.annotations.SerializedName

data class RegResponse(
    @SerializedName("error_text")
    var errorText: String,
)
