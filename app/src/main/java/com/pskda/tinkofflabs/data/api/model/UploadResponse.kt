package com.pskda.tinkofflabs.data.api.model

import com.google.gson.annotations.SerializedName

data class UploadResponse(
    @SerializedName("error_text")
    var errorText: String,
    @SerializedName("id")
    var contentId: Long,
    @SerializedName("size")
    var contentSize: Long,
    @SerializedName("type")
    var contentType: String
)
