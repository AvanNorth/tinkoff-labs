package com.pskda.tinkofflabs.data.api.model

data class Video(
    val URL: String,
    val author: String,
    val likes: Int
)