package com.pskda.tinkofflabs.data.api

import com.pskda.tinkofflabs.data.api.model.PostResponse
import com.pskda.tinkofflabs.data.api.model.UploadResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface UploadApi {
    @Multipart
    @POST("/avatar/upload")
    suspend fun uploadAvatar(@Part file: MultipartBody.Part): UploadResponse

    @Multipart
    @POST("/post/new")
    suspend fun uploadVideo(
        @Part video: MultipartBody.Part,
        @Part("description") description: RequestBody
    ): PostResponse
}