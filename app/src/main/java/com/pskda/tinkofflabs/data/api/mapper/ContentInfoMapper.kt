package com.pskda.tinkofflabs.data.api.mapper

import com.pskda.tinkofflabs.data.api.model.UploadResponse
import com.pskda.tinkofflabs.domain.entity.ContentInfo

class ContentInfoMapper {
    fun toContentInfo(response: UploadResponse): ContentInfo = ContentInfo(
        id = response.contentId,
        size = response.contentSize,
        type = response.contentType
    )
}