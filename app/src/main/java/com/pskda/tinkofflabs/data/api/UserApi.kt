package com.pskda.tinkofflabs.data.api

import com.pskda.tinkofflabs.data.api.model.User
import retrofit2.http.*

interface UserApi {
    @GET("profile/{id}")
    suspend fun getProfile(@Path("id") id: String): User

    @POST("subscriptions/subscribe")
    suspend fun subscribeById(@Query("authorId") authorId: Long): User

    @DELETE("subscriptions/unsubscribe")
    suspend fun unsubscribeById(@Query("authorId") authorId: Long): User
}