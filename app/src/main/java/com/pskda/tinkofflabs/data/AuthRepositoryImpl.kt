package com.pskda.tinkofflabs.data

import com.pskda.tinkofflabs.data.api.AuthApi
import com.pskda.tinkofflabs.data.api.model.LoginRequest
import com.pskda.tinkofflabs.data.api.model.LoginResponse
import com.pskda.tinkofflabs.data.api.model.RegRequest
import com.pskda.tinkofflabs.data.api.model.RegResponse
import com.pskda.tinkofflabs.domain.repository.AuthRepository
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val authApi: AuthApi
) : AuthRepository {
    override suspend fun logIn(loginRequest: LoginRequest): LoginResponse =
        authApi.logIn(loginRequest)

    override suspend fun signUp(regRequest: RegRequest): RegResponse =
        authApi.signUp(regRequest)
}
