package com.pskda.tinkofflabs.data.api

import com.pskda.tinkofflabs.data.api.model.LikeResponse
import com.pskda.tinkofflabs.data.api.model.PostListResponse
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface VideoApi {
    @GET("post/all")
    suspend fun getProfileVideos(
        @Query("page") page: Int,
        @Query("authorId") id: Long
    ): PostListResponse

    @GET("post/all")
    suspend fun getFeedVideos(@Query("page") page: Int): PostListResponse

    @GET("post/all/subscriptions")
    suspend fun getSubscriptionVideos(@Query("page") page: Int): PostListResponse

    @POST("post/{id}/putLike")
    suspend fun putLike(@Path("id") id: Long): LikeResponse

    @POST("post/{id}/removeLike")
    suspend fun removeLike(@Path("id") id: Long): LikeResponse
}