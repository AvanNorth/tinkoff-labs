package com.pskda.tinkofflabs.data.api.model

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id")
    var id: Long,
    @SerializedName("nick")
    var nickName: String,
    @SerializedName("subscriptionsCount")
    var subscriptions: Int,
    @SerializedName("subscribersCount")
    var subscribers: Int,
    @SerializedName("postsLikesSumCount")
    var likes: Int,
    @SerializedName("avatarId")
    var avatarId: Long,
    @SerializedName("isSubscribed")
    var isSubscribed: Boolean
)
