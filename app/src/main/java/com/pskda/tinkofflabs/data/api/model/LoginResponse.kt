package com.pskda.tinkofflabs.data.api.model

import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("error_text")
    var errorText: String,

    @SerializedName("token")
    var authToken: String
)
