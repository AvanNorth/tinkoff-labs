package com.pskda.tinkofflabs.data.api.mapper

import com.pskda.tinkofflabs.data.api.model.PostListResponse
import com.pskda.tinkofflabs.data.api.model.PostResponse
import com.pskda.tinkofflabs.domain.entity.Post
import com.pskda.tinkofflabs.domain.entity.FeedPostList

class VideoMapper {
    fun toPost(videoItem: PostResponse): Post = Post(
        id = videoItem.id,
        description = videoItem.description,
        authorId = videoItem.authorId,
        thumbId = videoItem.thumbId,
        videoId = videoItem.videoId,
        timeStamp = videoItem.timeStamp,
        author = videoItem.author,
        whoLiked = videoItem.likes,
        isLiked = false,
        likesCount = 0
    )

    fun toPostList(videoListResponse: PostListResponse): FeedPostList = FeedPostList(
        dataList = videoListResponse.videoList.map { userVideoItem -> toPost(userVideoItem) } as MutableList<Post>,
        totalPages = videoListResponse.totalPages,
        currentPage = videoListResponse.currentPage
    )
}