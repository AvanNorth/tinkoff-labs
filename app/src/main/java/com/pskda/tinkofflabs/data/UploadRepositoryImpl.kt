package com.pskda.tinkofflabs.data

import com.pskda.tinkofflabs.data.api.UploadApi
import com.pskda.tinkofflabs.data.api.mapper.ContentInfoMapper
import com.pskda.tinkofflabs.data.api.mapper.VideoMapper
import com.pskda.tinkofflabs.domain.entity.ContentInfo
import com.pskda.tinkofflabs.domain.entity.Post
import com.pskda.tinkofflabs.domain.entity.VideoPostItem
import com.pskda.tinkofflabs.domain.repository.UploadRepository
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject

class UploadRepositoryImpl @Inject constructor(
    private val api: UploadApi,
    private val mapper: ContentInfoMapper,
    private val videoMapper: VideoMapper
) : UploadRepository {
    override suspend fun uploadAvatar(avatar: File): ContentInfo = mapper.toContentInfo(
        api.uploadAvatar(buildMPB(avatar, "image/*"))
    )

    override suspend fun uploadVideo(videoPostItem: VideoPostItem): Post =
        videoMapper.toPost(
            api.uploadVideo(
                buildMPB(videoPostItem.video, "video/*"),
                videoPostItem.description.toRequestBody()
            )
        )

    private fun buildMPB(file: File, type: String): MultipartBody.Part {
        val requestFile = file.asRequestBody(type.toMediaTypeOrNull())
        return MultipartBody.Part.createFormData("file", file.name, requestFile)
    }
}