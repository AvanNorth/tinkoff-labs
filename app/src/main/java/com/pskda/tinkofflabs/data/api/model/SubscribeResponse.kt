package com.pskda.tinkofflabs.data.api.model

import com.google.gson.annotations.SerializedName

data class SubscribeResponse(
    @SerializedName("authorId")
    var authorId: Long,
    @SerializedName("subscriberId")
    var subscriberId: Long
)
