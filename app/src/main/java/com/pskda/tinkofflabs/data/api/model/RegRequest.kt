package com.pskda.tinkofflabs.data.api.model

import com.google.gson.annotations.SerializedName

data class RegRequest(
    @SerializedName("nick")
    var nick : String,

    @SerializedName("email")
    var email : String,

    @SerializedName("password")
    var password : String
)
