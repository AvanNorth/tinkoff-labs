package com.pskda.tinkofflabs.data.session

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import javax.inject.Inject

private val Context.dataStore by preferencesDataStore("user")

class UserPreferences (context: Context) {
    private val userDataStore = context.dataStore

    companion object {
        private val AUTH_KEY = stringPreferencesKey("auth_key")
    }

    val authToken: Flow<String?>
        get() = userDataStore.data.map { prefs ->
            prefs[AUTH_KEY]
        }

    suspend fun saveAuthToken(authToken: String) {
        userDataStore.edit { prefs ->
            prefs[AUTH_KEY] = authToken
        }
    }

    suspend fun getAuthToken(): String? = authToken.first()
}