package com.pskda.tinkofflabs.data.api.model

import com.google.gson.annotations.SerializedName

data class PostResponse(
    @SerializedName("id")
    var id: Long,
    @SerializedName("description")
    var description: String,
    @SerializedName("authorId")
    var authorId: Long,
    @SerializedName("thumbnailId")
    var thumbId: Long,
    @SerializedName("videoId")
    var videoId: Long,
    @SerializedName("createdAt")
    var timeStamp: String,
    @SerializedName("authorNick")
    var author: String,
    @SerializedName("whoLiked")
    var likes: List<Long>
)
