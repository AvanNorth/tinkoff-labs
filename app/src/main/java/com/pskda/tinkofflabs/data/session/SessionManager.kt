package com.pskda.tinkofflabs.data.session

import com.auth0.android.jwt.JWT
import com.pskda.tinkofflabs.domain.entity.SessionUser
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SessionManager @Inject constructor(
    private val userPrefs: UserPreferences
) {
    private val dispatcher: CoroutineDispatcher = Dispatchers.Main

    suspend fun getCurrentUser(): SessionUser {
        return withContext(dispatcher) {
            val jwt = JWT(userPrefs.getAuthToken().toString())
            SessionUser(jwt.subject.toString())
        }
    }

    suspend fun saveAuthToken(token: String) {
        return withContext(dispatcher) {
            userPrefs.saveAuthToken(token)
        }
    }

    suspend fun isAuth(): Boolean {
        return withContext(dispatcher) {
            userPrefs.getAuthToken() != null
        }
    }
}