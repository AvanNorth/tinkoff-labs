package com.pskda.tinkofflabs.data.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.pskda.tinkofflabs.R
import com.pskda.tinkofflabs.domain.entity.Post

class VideoGridAdapter(var context: Context, private val onItemClick: (dataList: MutableList<Post>, position: Int) -> Unit) :
    RecyclerView.Adapter<VideoGridAdapter.ViewHolder>() {
    private var dataList = mutableListOf<Post>()
    private var lastListSize = 0

    internal fun setDataList(dataList: MutableList<Post>) {
        this.dataList = dataList
        notifyItemRangeInserted(lastListSize, dataList.size)
        saveListSize()
    }

    class ViewHolder(itemView: View, private val onItemClick: (dataList: MutableList<Post>, position: Int) -> Unit, dataList: MutableList<Post>) :
        RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                onItemClick(dataList, absoluteAdapterPosition)
            }
        }

        var image: ImageView = itemView.findViewById(R.id.gridVideoThumb)
        var likes: TextView = itemView.findViewById(R.id.gridLikesTV)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.video_card, parent, false)
        return ViewHolder(view, onItemClick, dataList)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = dataList[position]
        holder.likes.text = data.likesCount.toString()

        holder.image.load(
            "https://stogramm.herokuapp.com/post/${data.id}/thumbnail"
        )
    }

    fun updateData(videos: MutableList<Post>) {
        dataList.addAll(videos)
        notifyItemRangeInserted(lastListSize, itemCount)
        saveListSize()
    }

    override fun getItemCount() = dataList.size

    private fun saveListSize() {
        lastListSize = dataList.size
    }
}