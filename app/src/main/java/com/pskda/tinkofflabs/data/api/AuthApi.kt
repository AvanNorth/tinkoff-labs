package com.pskda.tinkofflabs.data.api

import com.pskda.tinkofflabs.data.api.model.LoginRequest
import com.pskda.tinkofflabs.data.api.model.LoginResponse
import com.pskda.tinkofflabs.data.api.model.RegRequest
import com.pskda.tinkofflabs.data.api.model.RegResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {
    @POST("login")
    suspend fun logIn(@Body request: LoginRequest): LoginResponse

    @POST("signUp")
    suspend fun signUp(@Body request: RegRequest): RegResponse
}