package com.pskda.tinkofflabs.data

import com.pskda.tinkofflabs.data.api.VideoApi
import com.pskda.tinkofflabs.data.api.mapper.VideoMapper
import com.pskda.tinkofflabs.data.api.model.LikeResponse
import com.pskda.tinkofflabs.domain.entity.FeedPostList
import com.pskda.tinkofflabs.domain.repository.VideoRepository
import javax.inject.Inject

class VideoRepositoryImpl @Inject constructor(
    private val api: VideoApi,
    private val mapper: VideoMapper
) : VideoRepository {
    override suspend fun getVideosByAuthor(page: Int, id: Long): FeedPostList =
        mapper.toPostList(api.getProfileVideos(page, id))

    override suspend fun getFeedVideos(page: Int): FeedPostList =
        mapper.toPostList(api.getFeedVideos(page))

    override suspend fun getSubscriptionsVideos(page: Int): FeedPostList =
        mapper.toPostList(api.getSubscriptionVideos(page))

    override suspend fun putLike(id: Long): LikeResponse = api.putLike(id)

    override suspend fun removeLike(id: Long): LikeResponse = api.removeLike(id)
}