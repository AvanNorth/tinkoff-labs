package com.pskda.tinkofflabs.presentation.fragments.main

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.pskda.tinkofflabs.R
import com.pskda.tinkofflabs.databinding.FragmentFeedBinding
import com.pskda.tinkofflabs.domain.entity.FeedPostList
import com.pskda.tinkofflabs.domain.entity.Post
import com.pskda.tinkofflabs.presentation.player.ExoVideoPlayer
import com.pskda.tinkofflabs.presentation.utils.pagination.PaginationPlayer
import com.pskda.tinkofflabs.presentation.viewmodel.FeedViewModel
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class FeedFragment : Fragment() {
    private lateinit var binding: FragmentFeedBinding
    private lateinit var exoPlayer: ExoVideoPlayer
    private val viewModel: FeedViewModel by viewModels()
    private var currentPage = 0
    private var authorId = 0L
    private var totalPages = 0
    private var isLoading = false
    private var subsMod = false
    private var authorMod = false
    private lateinit var navController: NavController
    private val args: FeedFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFeedBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = NavHostFragment.findNavController(this)

        with(binding) {
            initExoPlayer(exoVideoView)
            initListeners()
            initObservers()
            checkArgs()
        }
    }

    private fun initExoPlayer(exoVideoView: StyledPlayerView) {
        exoPlayer = object : PaginationPlayer(exoVideoView, true) {
            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun isLastPage(): Boolean {
                return currentPage + 1 == totalPages
            }

            override fun loadNextPage() {
                currentPage++
                when {
                    subsMod -> viewModel.getSubscriptionsFeed(currentPage)
                    authorMod -> viewModel.getAuthorVideos(currentPage, authorId)
                    else -> viewModel.getFeed(currentPage)
                }
            }

            override fun putLikeOnPost(id: Long) {
                setIsLikeActive(true)
                viewModel.putLike(id)
            }

            override fun removeLikeFromPost(id: Long) {
                setIsLikeActive(false)
                viewModel.removeLike(id)
            }

            override fun showPostInfo() {
                setPostInfo(getVideoInfo())
            }
        }
    }

    private fun checkArgs() {
        val videoList = args.videoList
        val position = args.position

        if (position != -1 && videoList != null) {
            exoPlayer.setPlaylistData(videoList, position)
            authorMod = true
            subsMod = !authorMod
            with(binding) {
                feedModTV.visibility = View.GONE
                subsModTV.visibility = View.GONE
                userVideos.text =
                    String.format(getString(R.string.userVideos), videoList.list[0].author)
            }
        } else {
            binding.userVideos.visibility = View.GONE
            binding.feedModTV.typeface = Typeface.DEFAULT_BOLD
            viewModel.getFeed(currentPage)
        }
    }

    private fun initListeners() {
        with(binding) {
            feedModTV.setOnClickListener {
                if (subsMod) {
                    switchBoldStyle(subsModTV, feedModTV)
                    subsMod = false
                    resetPostInfo()
                    exoPlayer.resetPlayList()
                    viewModel.getFeed(0)
                }
            }

            subsModTV.setOnClickListener {
                if (!subsMod) {
                    switchBoldStyle(feedModTV, subsModTV)
                    subsMod = true
                    resetPostInfo()
                    exoPlayer.resetPlayList()
                    viewModel.getSubscriptionsFeed(0)
                }
            }

            leftBtn.setOnClickListener {
                exoPlayer.prevVideo()
            }

            rightBtn.setOnClickListener {
                (exoPlayer as PaginationPlayer).next()
            }

            authorTV.setOnClickListener {
                val action =
                    FeedFragmentDirections.actionFeedToAuthorProfile(exoPlayer.getVideoInfo().authorId)
                navController.navigate(action)
            }
        }
    }

    private fun initObservers() {
        viewModel.videoFeedPostList.observe(viewLifecycleOwner) { result ->
            result.fold(onSuccess = { postList ->
                initPlayerData(postList)
            },
                onFailure = {
                    Timber.e(it.message.toString())
                })
        }

        viewModel.videoLike.observe(viewLifecycleOwner) { result ->
            result.fold(onSuccess = { likes ->
                binding.videoLikesTV.text = likes.likes.toString()
            },
                onFailure = {
                    Timber.e(it.message.toString())
                })
        }
    }

    private fun initPlayerData(feedPostList: FeedPostList) {
        totalPages = feedPostList.totalPages
        if (currentPage == 0) {
            exoPlayer.setPlaylistData(feedPostList.dataList)
            exoPlayer.play()
        } else
            exoPlayer.addPlaylistData(feedPostList.dataList)
    }

    private fun setPostInfo(currentPost: Post) {
        resetPostInfo()
        binding.authorTV.text = currentPost.author
        setIsLikeActive(currentPost.isLiked)
        binding.videoLikesTV.text = currentPost.likesCount.toString()
        binding.descrTV.text = currentPost.description
    }

    private fun resetPostInfo() {
        setIsLikeActive(false)
        binding.authorTV.text = String()
        binding.videoLikesTV.text = String()
        binding.descrTV.text = String()
    }

    private fun switchBoldStyle(textViewFrom: TextView, textViewTo: TextView) {
        textViewFrom.typeface = Typeface.DEFAULT
        textViewTo.typeface = Typeface.DEFAULT_BOLD
    }

    private fun setIsLikeActive(active: Boolean) {
        with(binding) {
            videoLikesTV.setCompoundDrawablesWithIntrinsicBounds(
                if (active) {
                    R.drawable.ic_outline_favorite_border_24_active
                } else {
                    R.drawable.ic_outline_favorite_border_24_light
                }, 0, 0, 0
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        exoPlayer.stop()
        exoPlayer.release()
    }
}