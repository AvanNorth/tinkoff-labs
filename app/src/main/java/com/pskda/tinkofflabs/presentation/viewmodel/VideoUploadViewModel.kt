package com.pskda.tinkofflabs.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pskda.tinkofflabs.domain.entity.Post
import com.pskda.tinkofflabs.domain.entity.SessionUser
import com.pskda.tinkofflabs.domain.entity.VideoPostItem
import com.pskda.tinkofflabs.domain.usecase.SessionUseCase
import com.pskda.tinkofflabs.domain.usecase.UploadUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class VideoUploadViewModel @Inject constructor(
    private val useCase: UploadUseCase,
    private val sessionUseCase: SessionUseCase
) : ViewModel() {
    private var _uploadResponse: MutableLiveData<Result<Post>> = MutableLiveData()
    val uploadResponse: LiveData<Result<Post>> = _uploadResponse

    private var _sessionUser: MutableLiveData<Result<SessionUser>> = MutableLiveData()
    var sessionUser: LiveData<Result<SessionUser>> = _sessionUser

    fun uploadVideo(videoPostItem: VideoPostItem) = viewModelScope.launch {
        try {
            _uploadResponse.value = Result.success(useCase.uploadVideo(videoPostItem))
        } catch (e: Exception) {
            _uploadResponse.value = Result.failure(e)
        }
    }

    fun getSessionUser() = viewModelScope.launch {
        try {
            _sessionUser.value = Result.success(sessionUseCase.getCurrentUser())
        } catch (e: Exception) {
            _sessionUser.value = Result.failure(e)
        }
    }
}