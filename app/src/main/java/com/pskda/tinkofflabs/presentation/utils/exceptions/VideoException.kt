package com.pskda.tinkofflabs.presentation.utils.exceptions

class VideoException(message:String): Exception(message)