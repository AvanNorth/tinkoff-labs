package com.pskda.tinkofflabs.presentation.player

import android.os.Handler
import android.os.Looper
import android.view.View

class DoubleClickListener constructor(onDoubleClickListener: OnDoubleClickListener) :
    View.OnClickListener {

    private var interval: Long = 200L
    private var handler: Handler = Handler(Looper.getMainLooper()!!)
    private var onDoubleClickListener: OnDoubleClickListener? = null
    private var click = 0
    private var isBusy: Boolean = false

    init {
        this.onDoubleClickListener = onDoubleClickListener
    }

    interface OnDoubleClickListener {
        fun onClick()
        fun onDoubleClick()
    }

    override fun onClick(p0: View?) {
        if (!isBusy) {
            isBusy = true
            click++
            handler.postDelayed({
                if (click >= 2) {
                    onDoubleClickListener?.onDoubleClick()
                }
                if (click == 1) {
                    onDoubleClickListener?.onClick()
                }
                click = 0
            }, interval)
            isBusy = false
        }
    }
}