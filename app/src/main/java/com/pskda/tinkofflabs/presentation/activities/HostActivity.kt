package com.pskda.tinkofflabs.presentation.activities

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.pskda.tinkofflabs.presentation.viewmodel.HostViewModel
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class HostActivity : AppCompatActivity() {
    private val viewModel: HostViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initObservers()
        viewModel.checkSession()
    }

    private fun initObservers() {
        viewModel.session.observe(this) { result ->
            result.fold(onSuccess = { session ->
                val intent = if (session) {
                    Intent(this, MainActivity::class.java)
                } else {
                    Intent(this, AuthActivity::class.java)
                }
                startActivity(intent)
            },
                onFailure = { exception ->
                    Timber.e(exception.message.toString())
                }
            )
        }
    }
}