package com.pskda.tinkofflabs.presentation.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pskda.tinkofflabs.databinding.ActivityAuthBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthActivity : AppCompatActivity() {
    private lateinit var authBinding: ActivityAuthBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        authBinding = ActivityAuthBinding.inflate(layoutInflater)
        val view = authBinding.root
        setContentView(view)
    }
}