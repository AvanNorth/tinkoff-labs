package com.pskda.tinkofflabs.presentation.utils.pagination

import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.pskda.tinkofflabs.presentation.player.ExoVideoPlayer

abstract class PaginationPlayer(
    playerView: StyledPlayerView,
    likesAvailability: Boolean
) : ExoVideoPlayer(playerView, likesAvailability) {
    abstract fun isLoading(): Boolean
    abstract fun isLastPage(): Boolean
    abstract fun loadNextPage()
    abstract fun putLikeOnPost(id: Long)
    abstract fun removeLikeFromPost(id: Long)

    init {
        playerView.player?.addListener(object : Player.Listener {
            override fun onPlaybackStateChanged(state: Int) {
                if (state == Player.STATE_BUFFERING)
                    showPostInfo()
            }
        })
    }

    abstract fun showPostInfo()

    fun next() {
        if (isLastMediaItem()) {
            if (!(isLastPage() || isLoading()))
                loadNextPage()
            else
                return
        } else {
            nextVideo()
        }
    }

    override fun putLike() {
        putLikeOnPost(getVideoInfo().id)
    }

    override fun removeLike() {
        removeLikeFromPost(getVideoInfo().id)
    }
}