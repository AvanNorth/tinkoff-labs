package com.pskda.tinkofflabs.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pskda.tinkofflabs.domain.usecase.SessionUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HostViewModel @Inject constructor(
    private val sessionUseCase: SessionUseCase
) : ViewModel() {

    private var _session: MutableLiveData<Result<Boolean>> = MutableLiveData()
    val session: LiveData<Result<Boolean>> = _session

    fun checkSession() = viewModelScope.launch {
        try {
            _session.value = Result.success(sessionUseCase.isAuth())
        } catch (e: Exception) {
            _session.value = Result.failure(e)
        }
    }
}