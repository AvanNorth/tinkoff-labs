package com.pskda.tinkofflabs.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pskda.tinkofflabs.data.api.model.LoginRequest
import com.pskda.tinkofflabs.data.api.model.LoginResponse
import com.pskda.tinkofflabs.domain.usecase.AuthUseCase
import com.pskda.tinkofflabs.domain.usecase.SessionUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val authUseCase: AuthUseCase,
    private val sessionUseCase: SessionUseCase
) : ViewModel() {

    private var _loginResponse: MutableLiveData<Result<LoginResponse>> = MutableLiveData()
    val loginResponse: LiveData<Result<LoginResponse>> = _loginResponse

    fun logIn(request: LoginRequest) = viewModelScope.launch {
        try {
            _loginResponse.value = Result.success(authUseCase.logIn(request))
        } catch (e: Exception) {
            _loginResponse.value = Result.failure(e)
        }
    }

    fun saveToken(token: String) = viewModelScope.launch {
        sessionUseCase.saveAuthToken(token)
    }
}