package com.pskda.tinkofflabs.presentation.player

import android.net.Uri
import android.widget.Toast
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.pskda.tinkofflabs.domain.entity.Post
import com.pskda.tinkofflabs.domain.entity.PostList


open class ExoVideoPlayer(
    private val playerView: StyledPlayerView,
    private val likesAvailability: Boolean
) {
    private var exoVideoPlayer: SimpleExoPlayer =
        SimpleExoPlayer.Builder(playerView.context).build()

    init {
        playerView.player = exoVideoPlayer
        exoVideoPlayer.repeatMode = Player.REPEAT_MODE_ALL

        exoVideoPlayer.addListener(object : Player.Listener {
            override fun onPlayerError(error: PlaybackException) {
                Toast.makeText(playerView.context, "Ошибка воспроизведения", Toast.LENGTH_LONG)
                    .show()
                nextVideo()
            }
        })

        initControls(playerView)
        initOnClick(playerView)
        exoVideoPlayer.prepare()
    }

    fun setPlaylistData(postList: MutableList<Post>) {
        exoVideoPlayer.setMediaItems(generateMediaItemList(postList))
    }

    fun setPlaylistData(postList: PostList, position: Int) {
        exoVideoPlayer.setMediaItems(generateMediaItemList(postList.list))
        playParticularVideo(position)
    }

    fun setPlaylistData(uri: Uri) {
        exoVideoPlayer.setMediaItem(MediaItem.fromUri(uri))
    }

    fun addPlaylistData(postList: MutableList<Post>) {
        exoVideoPlayer.setMediaItems(generateMediaItemList(postList))
    }

    fun resetPlayList() {
        exoVideoPlayer.clearMediaItems()
    }

    private fun initControls(playerView: StyledPlayerView) {
        playerView.setShowNextButton(false)
        playerView.controllerShowTimeoutMs = 1000
        playerView.setShowPreviousButton(false)
    }

    private fun initOnClick(playerView: StyledPlayerView) {
        playerView.videoSurfaceView?.setOnClickListener(DoubleClickListener(object :
            DoubleClickListener.OnDoubleClickListener {
            override fun onClick() {
                if (exoVideoPlayer.isPlaying)
                    pause()
                else
                    play()
            }

            override fun onDoubleClick() {
                if (likesAvailability && getVideoInfo().isLiked)
                    removeLike()
                else if (likesAvailability)
                    putLike()
            }
        }))
    }

    private fun generateMediaItemList(postList: MutableList<Post>): MutableList<MediaItem> {
        val mediaItemList = mutableListOf<MediaItem>()

        postList.forEach { post ->
            mediaItemList.add(
                MediaItem.Builder()
                    .setUri("https://stogramm.herokuapp.com/post/${post.id}/video")
                    .setMediaId(post.id.toString())
                    .setTag(post).build()
            )
        }

        return mediaItemList
    }

    fun getVideoInfo(): Post {
        return exoVideoPlayer.currentMediaItem?.playbackProperties?.tag as Post
    }

    open fun putLike() {}

    open fun removeLike() {}

    fun nextVideo() {
        exoVideoPlayer.seekToNext()
        exoVideoPlayer.prepare()
        play()
    }

    fun prevVideo() {
        exoVideoPlayer.seekToPrevious()
        exoVideoPlayer.prepare()
        play()
    }

    fun play() {
        exoVideoPlayer.play()
    }

    fun pause() {
        exoVideoPlayer.pause()
    }

    fun release() {
        exoVideoPlayer.release()
    }

    fun stop() {
        exoVideoPlayer.stop()
    }

    fun playParticularVideo(position: Int) {
        exoVideoPlayer.seekTo(position, C.TIME_UNSET)
        exoVideoPlayer.prepare()
        play()
    }

    fun getDuration(): Long =
        exoVideoPlayer.duration / 1000

    fun isLastMediaItem(): Boolean {
        with(exoVideoPlayer) {
            return currentMediaItem == getMediaItemAt(mediaItemCount - 1)
        }
    }
}