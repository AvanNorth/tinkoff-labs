package com.pskda.tinkofflabs.presentation.fragments.main

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.pskda.tinkofflabs.BuildConfig
import com.pskda.tinkofflabs.R
import com.pskda.tinkofflabs.databinding.FragmentUploadBinding
import com.pskda.tinkofflabs.domain.entity.SessionUser
import com.pskda.tinkofflabs.domain.entity.VideoPostItem
import com.pskda.tinkofflabs.presentation.player.ExoVideoPlayer
import com.pskda.tinkofflabs.presentation.utils.exceptions.VideoException
import com.pskda.tinkofflabs.presentation.utils.extensions.ContextExtensions.deleteTmpFiles
import com.pskda.tinkofflabs.presentation.utils.extensions.ContextExtensions.hideKeyboard
import com.pskda.tinkofflabs.presentation.utils.extensions.ContextExtensions.visible
import com.pskda.tinkofflabs.presentation.viewmodel.VideoUploadViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.apache.commons.io.IOUtils
import timber.log.Timber
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.InputStream

private const val videoPrefix = "stogramm_tmp_video"


@AndroidEntryPoint
class VideoUploadFragment : Fragment() {
    private lateinit var binding: FragmentUploadBinding
    private var latestTmpUri: Uri? = null
    private val videoPreview by lazy { binding.exoVideoView }
    private lateinit var exoPlayer: ExoVideoPlayer
    private lateinit var videoUrl: Uri
    private lateinit var sessionUser: SessionUser
    private val viewModel: VideoUploadViewModel by viewModels()
    private lateinit var navController: NavController


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUploadBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        exoPlayer = ExoVideoPlayer(videoPreview, false)
        navController = NavHostFragment.findNavController(this)

        binding.progressBar.visible(false)
        videoPreview.visible(false)
        binding.cancelBtn.visible(false)

        viewModel.getSessionUser()
        initObservers()
        initClickListeners()
    }

    private fun initClickListeners() {
        with(binding) {
            uploadBtn.setOnClickListener {
                progressBar.visible(true)
                uploadBtn.visibility = View.INVISIBLE
                try {
                    checkDuration()
                } catch (e: VideoException) {
                    Timber.e(e.message.toString())
                }
                hideKeyboard()
            }
            galleryBtn.setOnClickListener {
                try {
                    selectVideoFromGallery()
                }catch (e: Exception){
                    Timber.e(e.fillInStackTrace())
                }
            }
            cameraBtn.setOnClickListener {
                try {
                    takeVideo()
                }catch (e: Exception){
                    Timber.e(e.fillInStackTrace())
                }
            }
            cancelBtn.setOnClickListener {
                videoUrl = Uri.EMPTY
                exoPlayer.resetPlayList()
                showButtons()
            }
        }
    }

    private fun initObservers() {
        viewModel.sessionUser.observe(viewLifecycleOwner) { result ->
            result.fold(onSuccess = { sessionUser ->
                this.sessionUser = sessionUser
            },
                onFailure = { exception ->
                    Timber.e(exception.message.toString())
                }
            )
        }

        viewModel.uploadResponse.observe(viewLifecycleOwner) { result ->
            result.fold(onSuccess = {
                navController.navigate(R.id.actionVideoUploadToFeed)
            },
                onFailure = { exception ->
                    binding.progressBar.visible(false)
                    Timber.e(exception.message.toString())
                }
            )
        }
    }

    private fun hideButtons() {
        with(binding) {
            videoPreview.visible(true)
            cancelBtn.visible(true)

            descTV.visible(false)
            galleryBtn.visible(false)
            cameraBtn.visible(false)
        }
    }

    private fun showButtons() {
        with(binding) {
            videoPreview.visible(false)
            cancelBtn.visible(false)

            descTV.visible(true)
            galleryBtn.visible(true)
            cameraBtn.visible(true)
        }
    }

    private fun checkDuration() {
        when {
            exoPlayer.getDuration() > 20 -> throw VideoException(getString(R.string.videoLengthError20))
            exoPlayer.getDuration() < 3 -> throw VideoException(getString(R.string.videoLengthError3))
            else -> try {
                uploadVideo(videoUrl)
            } catch (e: Exception) {
                Timber.e(e.message.toString())
            }
        }
    }

    private val takeVideoResult =
        registerForActivityResult(ActivityResultContracts.CaptureVideo()) { isSuccess ->
            if (isSuccess) {
                hideButtons()
                latestTmpUri?.let { uri ->
                    videoUrl = uri
                    exoPlayer.setPlaylistData(uri)
                }
            }
        }

    private val selectVideoFromGalleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            uri?.let {
                hideButtons()
                videoUrl = uri
                exoPlayer.setPlaylistData(uri)
            }
        }

    private fun takeVideo() {
        lifecycleScope.launchWhenStarted {
            getTmpFileUri().let { uri ->
                latestTmpUri = uri
                takeVideoResult.launch(uri)
            }
        }
    }

    private fun selectVideoFromGallery() = selectVideoFromGalleryResult.launch("video/*")

    private fun getTmpFileUri(): Uri {
        val tmpFile = File.createTempFile(videoPrefix, ".mp4").apply {
            createNewFile()
            deleteOnExit()
        }
        return FileProvider.getUriForFile(
            requireContext(),
            "${BuildConfig.APPLICATION_ID}.provider",
            tmpFile
        )
    }

    private fun createTmpFileForUpload(inp: InputStream): File {
        val tempFile = File.createTempFile(videoPrefix, ".mp4")
        tempFile.deleteOnExit()
        val out = FileOutputStream(tempFile)
        IOUtils.copy(inp, out)
        return tempFile
    }

    private fun uploadVideo(uri: Uri) {
        val input = requireContext().contentResolver.openInputStream(uri)
        if (input != null) {
            val videoItem = VideoPostItem(
                createTmpFileForUpload(input),
                sessionUser.id,
                binding.textInput.editText?.text.toString()
            )
            viewModel.uploadVideo(videoItem)
        } else throw FileNotFoundException()
    }
}