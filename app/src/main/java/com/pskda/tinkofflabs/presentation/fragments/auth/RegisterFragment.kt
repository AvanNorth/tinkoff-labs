package com.pskda.tinkofflabs.presentation.fragments.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.pskda.tinkofflabs.R
import com.pskda.tinkofflabs.data.api.model.RegRequest
import com.pskda.tinkofflabs.databinding.FragmentRegBinding
import com.pskda.tinkofflabs.presentation.utils.extensions.ContextExtensions.hideKeyboard
import com.pskda.tinkofflabs.presentation.utils.extensions.ContextExtensions.visible
import com.pskda.tinkofflabs.presentation.utils.validation.AuthValidator
import com.pskda.tinkofflabs.presentation.viewmodel.RegViewModel
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class RegisterFragment : Fragment() {
    private lateinit var regBinding: FragmentRegBinding
    private val viewModel: RegViewModel by viewModels()
    private lateinit var navController: NavController
    private val authValidator by lazy {
        AuthValidator()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        regBinding = FragmentRegBinding.inflate(layoutInflater)
        return regBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()

        navController = NavHostFragment.findNavController(this)
        with(regBinding) {
            regBtn.setOnClickListener {
                regBtn.isEnabled = false
                regPB.visible(true)
                hideKeyboard()
                signUp(getEmail(), getNick(), getPassword())
            }
        }
    }

    private fun getEmail(): String = regBinding.emailTextField.editText?.text.toString()
    private fun getNick(): String = regBinding.loginTextField.editText?.text.toString()
    private fun getPassword(): String = regBinding.passwordTextField.editText?.text.toString()

    private fun signUp(email: String, nickName: String, password: String) {
        with(regBinding) {
            if (authValidator.isValidEmail(email) && authValidator.isValidPassword(password)) {
                viewModel.signUp(RegRequest(nickName, email, password))
            } else if (!authValidator.isValidEmail(email)) {
                regBtn.isEnabled = true
                regPB.visible(false) //TODO Вынести в метод
                regEmailET.error = "Неккоректный емайл"
            } else {
                regBtn.isEnabled = true
                regPB.visible(false)
                regPasswordET.error = "Некорректный пароль"
            }
        }
    }

    private fun initObservers() {
        viewModel.regResponse.observe(viewLifecycleOwner) { result ->
            result.fold(onSuccess = {
                navController.navigate(R.id.actionRegToEmailFragment)
            },
                onFailure = {
                    with(regBinding) {
                        regPB.visible(false)
                        regPB.isEnabled = true
                        errorTv.text = it.message.toString()
                    }
                    Timber.e(it.message.toString())
                })
        }
    }
}