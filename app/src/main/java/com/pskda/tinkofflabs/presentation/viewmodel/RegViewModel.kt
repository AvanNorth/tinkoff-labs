package com.pskda.tinkofflabs.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pskda.tinkofflabs.data.api.model.RegRequest
import com.pskda.tinkofflabs.data.api.model.RegResponse
import com.pskda.tinkofflabs.domain.usecase.AuthUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.HttpException
import javax.inject.Inject

@HiltViewModel
class RegViewModel @Inject constructor(
    private val authUseCase: AuthUseCase
) : ViewModel() {
    private var _regResponse: MutableLiveData<Result<RegResponse>> = MutableLiveData()
    val regResponse: LiveData<Result<RegResponse>> = _regResponse

    fun signUp(request: RegRequest) = viewModelScope.launch {
        try {
            _regResponse.value = Result.success(authUseCase.signUp(request))
        } catch (e: HttpException) {
            _regResponse.value = Result.failure(e)
        }
    }
}