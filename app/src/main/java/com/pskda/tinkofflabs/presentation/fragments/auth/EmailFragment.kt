package com.pskda.tinkofflabs.presentation.fragments.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.pskda.tinkofflabs.R
import com.pskda.tinkofflabs.databinding.FragmentEmailBinding

class EmailFragment : Fragment() {
    private lateinit var emailBinding: FragmentEmailBinding
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        emailBinding = FragmentEmailBinding.inflate(layoutInflater)

        return emailBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = NavHostFragment.findNavController(this)


        with(emailBinding) {
            navToLoginBtn.setOnClickListener {
                navController.navigate(R.id.actionEmailToLoginFragment)
            }
        }
    }
}