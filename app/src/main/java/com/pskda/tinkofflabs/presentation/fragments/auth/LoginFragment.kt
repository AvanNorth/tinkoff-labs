package com.pskda.tinkofflabs.presentation.fragments.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.pskda.tinkofflabs.R
import com.pskda.tinkofflabs.data.api.model.LoginRequest
import com.pskda.tinkofflabs.data.api.model.RegRequest
import com.pskda.tinkofflabs.databinding.FragmentLoginBinding
import com.pskda.tinkofflabs.presentation.utils.extensions.ContextExtensions.hideKeyboard
import com.pskda.tinkofflabs.presentation.utils.extensions.ContextExtensions.visible
import com.pskda.tinkofflabs.presentation.utils.validation.AuthValidator
import com.pskda.tinkofflabs.presentation.viewmodel.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class LoginFragment : Fragment() {
    private lateinit var loginBinding: FragmentLoginBinding
    private val viewModel: LoginViewModel by viewModels()
    private lateinit var navController: NavController
    private val authValidator by lazy{
        AuthValidator()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        loginBinding = FragmentLoginBinding.inflate(layoutInflater)

        return loginBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = NavHostFragment.findNavController(this)

        initObservers()
        with(loginBinding) {
            loginBtn.setOnClickListener {
                loginPB.visible(true)
                loginBtn.isEnabled = false
                hideKeyboard()
                login(getEmail(), getPassword())
            }
            navToRegBtn.setOnClickListener {
                navController.navigate(R.id.actionLoginToRegFragment)
            }
        }
    }

    private fun getEmail(): String = loginBinding.authLoginET.text.toString()

    private fun getPassword(): String = loginBinding.authPasswordET.text.toString()

    private fun login(email: String, password: String) {
        with(loginBinding) {
            if (authValidator.isValidEmail(email) && authValidator.isValidPassword(password)) {
                viewModel.logIn(LoginRequest(email, password))
            } else if (!authValidator.isValidEmail(email)) {
                loginBtn.isEnabled = true
                loginPB.visible(false) //TODO Вынести в метод
                authLoginET.error = "Неккоректный емайл"
            } else {
                loginBtn.isEnabled = true
                loginPB.visible(false)
                authPasswordET.error = "Некорректный пароль"
            }
        }
    }

    private fun initObservers() {
        viewModel.loginResponse.observe(viewLifecycleOwner) { result ->
            result.fold(onSuccess = { response ->
                viewModel.saveToken(response.authToken)
                navController.navigate(R.id.actionLoginToMainFragment)
            },
                onFailure = {
                    with(loginBinding) {
                        loginPB.isEnabled = true
                        loginPB.visible(false)
                        errorTv.text = it.message.toString()
                    }
                    Timber.e(it.message.toString())
                })
        }
    }
}