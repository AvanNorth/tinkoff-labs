package com.pskda.tinkofflabs.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pskda.tinkofflabs.data.api.model.LikeResponse
import com.pskda.tinkofflabs.domain.entity.FeedPostList
import com.pskda.tinkofflabs.domain.usecase.VideoUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FeedViewModel @Inject constructor(
    private val videoUseCase: VideoUseCase
) : ViewModel() {

    private var _videoFeedPostList: MutableLiveData<Result<FeedPostList>> = MutableLiveData()
    val videoFeedPostList: LiveData<Result<FeedPostList>> = _videoFeedPostList

    private var _videoLike: MutableLiveData<Result<LikeResponse>> = MutableLiveData()
    val videoLike: LiveData<Result<LikeResponse>> = _videoLike

    fun getFeed(page: Int) = viewModelScope.launch {
        try {
            _videoFeedPostList.value = Result.success(videoUseCase.getFeed(page))
        } catch (e: Exception) {
            _videoFeedPostList.value = Result.failure(e)
        }
    }

    fun getSubscriptionsFeed(page: Int) = viewModelScope.launch {
        try {
            _videoFeedPostList.value = Result.success(videoUseCase.getSubsFeed(page))
        } catch (e: Exception) {
            _videoFeedPostList.value = Result.failure(e)
        }
    }

    fun getAuthorVideos(page: Int, authorId: Long) = viewModelScope.launch{
        try {
            _videoFeedPostList.value = Result.success(videoUseCase.getVideosByAuthorId(page,authorId))
        } catch (e: Exception) {
            _videoFeedPostList.value = Result.failure(e)
        }
    }

    fun putLike(id: Long) = viewModelScope.launch {
        try {
            _videoLike.value = Result.success(videoUseCase.putLike(id))
        } catch (e: Exception){
            _videoLike.value = Result.failure(e)
        }
    }

    fun removeLike(id: Long) = viewModelScope.launch {
        try {
            _videoLike.value = Result.success(videoUseCase.removeLike(id))
        } catch (e: Exception){
            _videoLike.value = Result.failure(e)
        }
    }
}