package com.pskda.tinkofflabs.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pskda.tinkofflabs.data.api.model.SubscribeResponse
import com.pskda.tinkofflabs.data.api.model.User
import com.pskda.tinkofflabs.domain.entity.ContentInfo
import com.pskda.tinkofflabs.domain.entity.FeedPostList
import com.pskda.tinkofflabs.domain.entity.SessionUser
import com.pskda.tinkofflabs.domain.usecase.SessionUseCase
import com.pskda.tinkofflabs.domain.usecase.UploadUseCase
import com.pskda.tinkofflabs.domain.usecase.UserUseCase
import com.pskda.tinkofflabs.domain.usecase.VideoUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.io.File
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val userUseCase: UserUseCase,
    private val sessionUseCase: SessionUseCase,
    private val videoUseCase: VideoUseCase,
    private val uploadUseCase: UploadUseCase
) : ViewModel() {
    private var _profileResponse: MutableLiveData<Result<User>> = MutableLiveData()
    var profileResponse: LiveData<Result<User>> = _profileResponse

    private var _sessionUser: MutableLiveData<Result<SessionUser>> = MutableLiveData()
    var sessionUser: LiveData<Result<SessionUser>> = _sessionUser

    private var _uploadAvatar: MutableLiveData<Result<ContentInfo>> = MutableLiveData()
    var uploadAvatar: LiveData<Result<ContentInfo>> = _uploadAvatar

    private var _videoGridData: MutableLiveData<Result<FeedPostList>> = MutableLiveData()
    var videoGridData: LiveData<Result<FeedPostList>> = _videoGridData

    fun getProfileById(id: String) = viewModelScope.launch {
        try {
            _profileResponse.value = Result.success(userUseCase.getProfileById(id))
        } catch (e: Exception) {
            _profileResponse.value = Result.failure(e)
        }
    }

    fun getSessionUser() = viewModelScope.launch {
        try {
            _sessionUser.value = Result.success(sessionUseCase.getCurrentUser())
        } catch (e: Exception) {
            _sessionUser.value = Result.failure(e)
        }
    }

    fun uploadAvatar(avatar: File) = viewModelScope.launch {
        try {
            _uploadAvatar.value = Result.success(uploadUseCase.uploadAvatar(avatar))
        } catch (e: Exception) {
            _uploadAvatar.value = Result.failure(e)
        }
    }

    fun getUserVideos(page: Int, id: Long) = viewModelScope.launch {
        try {
            _videoGridData.value = Result.success(videoUseCase.getVideosByAuthorId(page, id))
        } catch (e: Exception) {
            _videoGridData.value = Result.failure(e)
        }
    }

    fun subscribeToUser(id: Long) = viewModelScope.launch {
        try {
            _profileResponse.value = Result.success(userUseCase.subscribeToUser(id))
        } catch (e: Exception) {
            _profileResponse.value = Result.failure(e)
        }
    }

    fun unsubscribeFromUser(id: Long) = viewModelScope.launch {
        try {
            _profileResponse.value = Result.success(userUseCase.unsubscribeFromUser(id))
        } catch (e: Exception) {
            _profileResponse.value = Result.failure(e)
        }
    }
}