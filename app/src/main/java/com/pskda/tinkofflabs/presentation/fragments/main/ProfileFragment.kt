package com.pskda.tinkofflabs.presentation.fragments.main

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import coil.load
import coil.transform.CircleCropTransformation
import com.pskda.tinkofflabs.data.adapter.VideoGridAdapter
import com.pskda.tinkofflabs.data.api.model.User
import com.pskda.tinkofflabs.databinding.FragmentProfileBinding
import com.pskda.tinkofflabs.domain.entity.Post
import com.pskda.tinkofflabs.domain.entity.PostList
import com.pskda.tinkofflabs.domain.entity.SessionUser
import com.pskda.tinkofflabs.presentation.utils.extensions.ContextExtensions.deleteTmpFiles
import com.pskda.tinkofflabs.presentation.utils.extensions.ContextExtensions.visible
import com.pskda.tinkofflabs.presentation.utils.pagination.PaginationScrollListener
import com.pskda.tinkofflabs.presentation.viewmodel.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.apache.commons.io.IOUtils
import timber.log.Timber
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.InputStream

private const val imagePrefix = "stogramm_tmp_image"

@AndroidEntryPoint
class ProfileFragment : Fragment() {
    private lateinit var binding: FragmentProfileBinding
    private val viewModel: ProfileViewModel by viewModels()
    private lateinit var user: User
    private lateinit var videoGridAdapter: VideoGridAdapter
    private var isLoading = false
    private var currentPage = 0
    private var totalPages = 0
    private var authorMode = false
    private var isSubscribed = false
    private val args: ProfileFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        videoGridAdapter = VideoGridAdapter(requireContext()) { dataList, position ->
            showVideoFromGrid(dataList, position)
        }

        initObserves()
        initGrid()
        setClickListeners()
        viewModel.getSessionUser()
    }

    private fun setClickListeners() {
        with(binding) {
            editBtn.setOnClickListener {
                selectImageFromGallery()
            }

            subBtn.setOnClickListener {
                viewModel.subscribeToUser(user.id)
            }

            unsubBtn.setOnClickListener {
                viewModel.unsubscribeFromUser(user.id)
            }
        }
    }

    private fun checkArgs(currentUser: SessionUser) {
        val userId = args.profileId
        if (userId != -1L && currentUser.id != userId.toString()) {
            authorMode = true
            viewModel.getProfileById(userId.toString())
        } else
            viewModel.getProfileById(currentUser.id)
        switchBtn()
    }

    private fun initObserves() {
        viewModel.sessionUser.observe(viewLifecycleOwner) { result ->
            result.fold(onSuccess = { sessionUser ->
                checkArgs(sessionUser)
            },
                onFailure = {
                    Timber.e(it.message.toString())
                }
            )
        }

        viewModel.profileResponse.observe(viewLifecycleOwner) { result ->
            result.fold(onSuccess = { user ->
                this.user = user
                this.isSubscribed = user.isSubscribed
                initUsersCred(user)
                viewModel.getUserVideos(0, user.id)
            },
                onFailure = {
                    Timber.e(it.message.toString())
                })
        }

        viewModel.uploadAvatar.observe(viewLifecycleOwner) { result ->
            result.fold(onSuccess = { avatar ->
                user.avatarId = avatar.id
                initUsersCred(user)
            },
                onFailure = {
                    Timber.e(it.message.toString())
                }
            )
        }

        viewModel.videoGridData.observe(viewLifecycleOwner) { result ->
            result.fold(onSuccess = { data ->
                totalPages = data.totalPages
                isLoading = if (data.currentPage == 0) {
                    videoGridAdapter.setDataList(data.dataList)
                    false
                } else {
                    videoGridAdapter.updateData(data.dataList)
                    false
                }
            },
                onFailure = {
                    Timber.e(it.message.toString())
                })
        }
    }

    private val selectImageFromGalleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            uri?.let {
                binding.avatarIV.load(uri) {
                    crossfade(true)
                    transformations(CircleCropTransformation())
                }
                uploadAvatar(uri)
            }
        }

    private fun uploadAvatar(uri: Uri) {
        val input = requireContext().contentResolver.openInputStream(uri)
        if (input != null)
            viewModel.uploadAvatar(createTmpFileForUpload(input))
        else throw FileNotFoundException()
    }

    private fun initUsersCred(user: User) {
        with(binding) {
            switchBtn()
            nickTv.text = user.nickName
            subscriptionsTV.text = user.subscriptions.toString()
            subscribersTV.text = user.subscribers.toString()
            likesTv.text = user.likes.toString()
            avatarIV.load("https://stogramm.herokuapp.com/avatar/${user.avatarId}") {
                crossfade(true)
                transformations(CircleCropTransformation())
            }
        }
    }

    private fun initGrid() {
        with(binding) {
            val layoutManager = GridLayoutManager(requireContext(), 3)
            videoGrid.layoutManager = layoutManager
            videoGrid.adapter = videoGridAdapter

            videoGrid.addOnScrollListener(object :
                PaginationScrollListener(layoutManager) {
                override fun isLoading(): Boolean {
                    return isLoading
                }

                override fun loadNextPage() {
                    isLoading = true
                    viewModel.getUserVideos(currentPage + 1, user.id)
                    currentPage++
                }

                override fun isLastPage(): Boolean {
                    return currentPage + 1 == totalPages
                }
            })
        }
    }

    private fun showVideoFromGrid(dataList: MutableList<Post>, position: Int) {
        val action = ProfileFragmentDirections.actionShowAuthorVideos(PostList(dataList), position)
        findNavController().navigate(action)
    }

    private fun switchBtn() {
        with(binding) {
            when {
                authorMode && isSubscribed -> {
                    subBtn.visible(false)
                    editBtn.visible(false)
                    unsubBtn.visible(true)
                }
                authorMode && !isSubscribed -> {
                    editBtn.visible(false)
                    unsubBtn.visible(false)
                    subBtn.visible(true)
                }
                else -> {
                    subBtn.visible(false)
                    unsubBtn.visible(false)
                }
            }
        }
    }

    private fun createTmpFileForUpload(inp: InputStream): File {
        val tempFile = File.createTempFile(imagePrefix, ".png")
        tempFile.deleteOnExit()
        val out = FileOutputStream(tempFile)
        IOUtils.copy(inp, out)
        return tempFile
    }

    private fun selectImageFromGallery() = selectImageFromGalleryResult.launch("image/*")
}
