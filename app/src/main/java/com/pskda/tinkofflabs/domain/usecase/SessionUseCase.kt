package com.pskda.tinkofflabs.domain.usecase

import com.pskda.tinkofflabs.data.session.SessionManager
import com.pskda.tinkofflabs.domain.entity.SessionUser
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SessionUseCase @Inject constructor(
    private val sessionManager: SessionManager
) {
    private val dispatcher: CoroutineDispatcher = Dispatchers.Main

    suspend fun isAuth(): Boolean =
        withContext(dispatcher) {
            sessionManager.isAuth()
        }

    suspend fun saveAuthToken(token: String) =
        withContext(dispatcher) {
            sessionManager.saveAuthToken(token)
        }

    suspend fun getCurrentUser(): SessionUser =
        withContext(dispatcher) {
            sessionManager.getCurrentUser()
        }
}