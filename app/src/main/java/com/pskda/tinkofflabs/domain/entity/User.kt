package com.pskda.tinkofflabs.domain.entity

data class User(
    val id: String,
    val name: String
)
