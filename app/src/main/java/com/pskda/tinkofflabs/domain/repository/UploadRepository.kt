package com.pskda.tinkofflabs.domain.repository

import com.pskda.tinkofflabs.domain.entity.ContentInfo
import com.pskda.tinkofflabs.domain.entity.Post
import com.pskda.tinkofflabs.domain.entity.VideoPostItem
import java.io.File

interface UploadRepository {
    suspend fun uploadAvatar(avatar: File): ContentInfo
    suspend fun uploadVideo(videoPostItem: VideoPostItem): Post
}