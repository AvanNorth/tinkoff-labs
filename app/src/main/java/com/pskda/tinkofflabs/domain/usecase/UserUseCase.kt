package com.pskda.tinkofflabs.domain.usecase

import com.pskda.tinkofflabs.data.api.model.SubscribeResponse
import com.pskda.tinkofflabs.data.api.model.User
import com.pskda.tinkofflabs.domain.repository.ProfileRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UserUseCase @Inject constructor(
    private val repository: ProfileRepository
) {
    private val dispatcher: CoroutineDispatcher = Dispatchers.Main

    suspend fun getProfileById(id: String): User =
        withContext(dispatcher) {
            repository.getProfileById(id)
        }


    suspend fun subscribeToUser(id: Long): User =
        withContext(dispatcher) {
            repository.subscribeToUser(id)
        }


    suspend fun unsubscribeFromUser(id: Long): User =
        withContext(dispatcher) {
            repository.unsubscribeFromUser(id)
        }
}