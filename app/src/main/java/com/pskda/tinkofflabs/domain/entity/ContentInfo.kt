package com.pskda.tinkofflabs.domain.entity

data class ContentInfo(
    val id: Long,
    val size: Long,
    val type: String
)