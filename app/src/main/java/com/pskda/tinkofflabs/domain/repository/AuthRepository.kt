package com.pskda.tinkofflabs.domain.repository

import com.pskda.tinkofflabs.data.api.model.LoginRequest
import com.pskda.tinkofflabs.data.api.model.LoginResponse
import com.pskda.tinkofflabs.data.api.model.RegRequest
import com.pskda.tinkofflabs.data.api.model.RegResponse

interface AuthRepository {
    suspend fun logIn(loginRequest: LoginRequest): LoginResponse
    suspend fun signUp(regRequest: RegRequest): RegResponse
}