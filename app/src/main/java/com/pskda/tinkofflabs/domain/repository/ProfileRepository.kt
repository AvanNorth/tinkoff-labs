package com.pskda.tinkofflabs.domain.repository

import com.pskda.tinkofflabs.data.api.model.SubscribeResponse
import com.pskda.tinkofflabs.data.api.model.User

interface ProfileRepository {
    suspend fun getProfileById(id: String): User
    suspend fun subscribeToUser(id: Long): User
    suspend fun unsubscribeFromUser(id: Long): User
}