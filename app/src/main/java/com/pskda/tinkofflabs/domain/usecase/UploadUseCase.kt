package com.pskda.tinkofflabs.domain.usecase

import com.pskda.tinkofflabs.domain.entity.ContentInfo
import com.pskda.tinkofflabs.domain.entity.Post
import com.pskda.tinkofflabs.domain.entity.VideoPostItem
import com.pskda.tinkofflabs.domain.repository.UploadRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import javax.inject.Inject

class UploadUseCase @Inject constructor(
    private val uploadRepository: UploadRepository
) {
    private val dispatcher: CoroutineDispatcher = Dispatchers.Main

    suspend fun uploadAvatar(avatar: File): ContentInfo =
        withContext(dispatcher) {
            uploadRepository.uploadAvatar(avatar)
        }


    suspend fun uploadVideo(videoPost: VideoPostItem): Post =
        withContext(dispatcher) {
            uploadRepository.uploadVideo(videoPost)
        }

}