package com.pskda.tinkofflabs.domain.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PostList(
    val list: MutableList<Post>
) : Parcelable
