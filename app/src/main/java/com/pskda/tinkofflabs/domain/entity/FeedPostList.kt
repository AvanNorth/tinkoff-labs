package com.pskda.tinkofflabs.domain.entity

data class FeedPostList(
    val dataList: MutableList<Post>,
    val totalPages: Int,
    val currentPage: Int
)
