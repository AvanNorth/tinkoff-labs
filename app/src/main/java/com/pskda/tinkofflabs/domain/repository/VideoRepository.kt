package com.pskda.tinkofflabs.domain.repository

import com.pskda.tinkofflabs.data.api.model.LikeResponse
import com.pskda.tinkofflabs.domain.entity.FeedPostList

interface VideoRepository {
    suspend fun getVideosByAuthor(page: Int, id: Long): FeedPostList
    suspend fun getFeedVideos(page: Int): FeedPostList
    suspend fun getSubscriptionsVideos(page: Int): FeedPostList
    suspend fun putLike(id: Long): LikeResponse
    suspend fun removeLike(id: Long): LikeResponse
}