package com.pskda.tinkofflabs.domain.entity

data class SessionUser(
    val id: String
)
