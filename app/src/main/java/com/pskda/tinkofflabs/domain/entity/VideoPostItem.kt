package com.pskda.tinkofflabs.domain.entity

import java.io.File

data class VideoPostItem(
    val video: File,
    val author: String,
    val description: String
)