package com.pskda.tinkofflabs.domain.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Post(
    var id: Long,
    var description: String,
    var authorId: Long,
    var thumbId: Long,
    var videoId: Long,
    var timeStamp: String,
    var author: String,
    var whoLiked: List<Long>,
    var isLiked: Boolean,
    var likesCount: Int
) : Parcelable
