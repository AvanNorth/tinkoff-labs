package com.pskda.tinkofflabs.domain.usecase

import com.pskda.tinkofflabs.data.api.model.LikeResponse
import com.pskda.tinkofflabs.domain.entity.FeedPostList
import com.pskda.tinkofflabs.domain.entity.SessionUser
import com.pskda.tinkofflabs.domain.repository.VideoRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class VideoUseCase @Inject constructor(
    private val repository: VideoRepository,
    private val sessionUseCase: SessionUseCase
) {
    private val dispatcher: CoroutineDispatcher = Dispatchers.Main

    suspend fun getVideosByAuthorId(page: Int, id: Long): FeedPostList {
        var feed: FeedPostList
        val currentUser: SessionUser
        withContext(dispatcher) {
            feed = repository.getVideosByAuthor(page, id)
            currentUser = sessionUseCase.getCurrentUser()
        }
        return setLikesFields(feed, currentUser)
    }

    suspend fun getFeed(page: Int): FeedPostList {
        var feed: FeedPostList
        var currentUser: SessionUser
        withContext(dispatcher) {
            feed = repository.getFeedVideos(page)
            currentUser = sessionUseCase.getCurrentUser()
        }
        return setLikesFields(feed, currentUser)
    }

    suspend fun getSubsFeed(page: Int): FeedPostList {
        var feed: FeedPostList
        var currentUser: SessionUser
        withContext(dispatcher) {
            feed = repository.getSubscriptionsVideos(page)
            currentUser = sessionUseCase.getCurrentUser()
        }
        return setLikesFields(feed, currentUser)
    }

    suspend fun putLike(id: Long): LikeResponse =
        withContext(dispatcher) {
            repository.putLike(id)
        }

    suspend fun removeLike(id: Long): LikeResponse =
        withContext(dispatcher) {
            repository.removeLike(id)
        }


    private fun setLikesFields(postList: FeedPostList, curUs: SessionUser): FeedPostList {
        postList.dataList.stream().forEach { post ->
            post.likesCount = post.whoLiked.size
            post.isLiked = post.whoLiked.contains(curUs.id.toLong())
        }
        return postList
    }
}