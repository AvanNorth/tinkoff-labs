package com.pskda.tinkofflabs.domain.usecase

import com.pskda.tinkofflabs.data.api.model.LoginRequest
import com.pskda.tinkofflabs.data.api.model.LoginResponse
import com.pskda.tinkofflabs.data.api.model.RegRequest
import com.pskda.tinkofflabs.data.api.model.RegResponse
import com.pskda.tinkofflabs.domain.repository.AuthRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AuthUseCase @Inject constructor(
    private val authRep: AuthRepository
) {
    private val dispatcher: CoroutineDispatcher = Dispatchers.Main

    suspend fun logIn(request: LoginRequest): LoginResponse =
        withContext(dispatcher) {
            authRep.logIn(request)

        }

    suspend fun signUp(request: RegRequest): RegResponse =
        withContext(dispatcher) {
            authRep.signUp(request)
        }

}